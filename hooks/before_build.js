const fs = require("fs-extra");
const config = require('../gianniconfig.json');
fs.accessSync(__dirname + '/../gianniconfig.json');
if (typeof config.game != 'string') {
  console.error('[copier] please specify in gianniconfig.json the value "game" with a path to the base code');
  throw 'copier error, please see log';
}
console.log('[copier] emptying game directory');
fs.emptyDirSync(__dirname + '/../www');
fs.ensureFile(__dirname + '/../www/.gitkeep');
console.log('[copier] copying game code');
fs.copySync(config.game, __dirname + '/../www');
console.log('[copier] removing .git directory');
fs.removeSync(__dirname + '/../www/.git');
console.log('[copier] copying platform-specific modifications');
fs.copySync(__dirname + '/../gianniapp-www', __dirname + '/../www');
console.log('[copier] done');

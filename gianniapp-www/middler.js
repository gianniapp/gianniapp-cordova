version.middler = '2.0.1';
middlerName = 'gianniapp-cordova';
opensource['gianniapp-cordova'] = opensourcelicenses.mit('Marco Benzoni', 2018);
opensource['cordova'] = opensourcelicenses.apache('The Apache Software Foundation', '2012, 2013, 2015');
baseUrl = '';

var waitingForReady = {};
var isReady = false;

function handleOpenURL(url) {
  if (url.startsWith('gianniappcordova://sync-setup/?') || url.startsWith('gianniappcordova://sync-setup?')) {
    // gianniSync setup return URL
    var query = url.split('?')[1];
    query = query.split('&');
    var code = null;
    for (var queryPartStr of query) {
      var queryPart = queryPartStr.split('=');
      if (decodeURIComponent(queryPart[0]).toLowerCase() == 'giannisyncsetupcode') {
        code = decodeURIComponent(queryPart[1]);
      }
    }
    if (code != null) {
      var dataStr = atob(code);
      var data = JSON.parse(dataStr);
      if (isReady) {
        setupSync(data);
        $('#syncSetupServer')[0].close();
      } else waitingForReady['sync-setup'] = data;
    }
  }
};

var middler = {
  features: {
    externalDarkMode: false,
    packageManagement: false,
    syncRedirect: true
  },
  getFeatureInfo: function(name) {
    if (name == 'syncRedirect') {
      return 'gianniappcordova://sync-setup';
    }
  },
  getLang: function() {
    if (window.localStorage.getItem('middlerLang') == '') {
      var goToLang = knownLangs[0];
      if (typeof navigator.language != 'undefined') {
        var desiredLang = navigator.language.split('-')[0].toLowerCase();
        if (knownLangs.indexOf(desiredLang) > -1) {
          goToLang = desiredLang;
        }
      }
      window.localStorage.setItem('middlerLang', goToLang);
      return goToLang;
    } else {
      return window.localStorage.getItem('middlerLang');
    }
  },
  setLang: function(lang) {
    window.localStorage.setItem('middlerLang', lang);
  },
  dialog: function(title, text, button, callback) {
    navigator.notification.alert(text, callback, title, button);
  },
  dialogMultiple: function(title, text, btns, callback) {
    var correctingCallback = function(callback) {
      return function(i) {
        if (i == 0) {
          callback(0);
        } else {
          callback(i-1);
        }
      }
    }
    navigator.notification.confirm(text, correctingCallback(callback), title, btns);
  },
  dialogText: function(title, text, okBtn, cancelBtn, callback) {
    var promptCallback = function(callback) {
      return function(result) {
        if (result.buttonIndex == 1) {
          callback(null);
        } else {
          callback(result.input1);
        }
      }
    }
    navigator.notification.prompt(text, promptCallback(callback), title, [ cancelBtn, okBtn ], '');
  },
  url: function(url) {
    cordova.plugins.browsertab.isAvailable(function(result) {
      if (!result) {
        cordova.InAppBrowser.open(url, '_system');
      } else {
        cordova.plugins.browsertab.openUrl(
          url,
          function(successResp) {},
          function(failureResp) {
            cordova.InAppBrowser.open(url, '_system');
          }
        );
      }
    },
    function(isAvailableError) {
      cordova.InAppBrowser.open(url, '_system');
    });
  },
  quit: function() {
    navigator.app.exitApp();
  },
  data: {
    get: function(key) {
      return window.localStorage.getItem(key)
    },
    set: function(key, value) {
      return window.localStorage.setItem(key, value)
    },
    delete: function(key) {
      if (key === null) {
        return window.localStorage.clear();
      } else {
        return window.localStorage.removeItem(key)
      }
    }
  }
}

loadScript('cordova', 'cordova.js');

document.addEventListener('deviceready', function() {
  document.addEventListener("backbutton", function() {
    middler.exit();
  });
  window.dispatchEvent(new Event('framework-ready'));
});

window.addEventListener('game-ready', function() {
  isReady = true;
  if (waitingForReady['sync-setup']) setupSync(waitingForReady['sync-setup']);
});

// migration from previous data storage
window.addEventListener('game-ready', function() {
  var changedSomething = false;
  var fruitConvert = {
    '-2': 'vanilla',
    '-1': 'coin',
    '0': ['gianniapp', 'apple'],
    '1': ['gianniapp', 'pear'],
    '2': ['gianniapp', 'peach'],
    '3': ['gianniapp', 'apricot'],
    '4': ['gianniapp', 'grapes'],
    '5': ['gianniapp', 'watermelon'],
    '6': ['gianniapp', 'banana']
  }
  for (var i = -2; i <= 6; i++) {
    var amount = window.localStorage.getItem('fruit' + i);
    if (amount != null) {
      var convert = fruitConvert[i.toString()];
      if (convert === 'vanilla') {
        var data = getUserData(true);
        data.vanillaFruits = amount;
        setUserData(data, true);
      } else if (convert === 'coin') {
        setCoins(amount);
      } else {
        try {
          setFruit(convert[0], convert[1], amount);
        } catch (ex) {
          console.log('error in migrating fruits (package probably isn\'t loaded):', ex);
        }
      }
      changedSomething = true;
      window.localStorage.removeItem('fruit' + i);
    }
  }
  for (var i = 1; i <= 6; i++) {
    var bought = window.localStorage.getItem('shopBoughtfruits' + i);
    if (bought == 'true') {
      var convert = fruitConvert[i.toString()];
      try {
        var data = getUserData(true);
        if (!data.purchases.fruits[convert[0]]) {
          data.purchases.fruits[convert[0]] = {};
        }
        data.purchases.fruits[convert[0]][convert[1]] = true;
        setUserData(data, true);
      } catch (ex) {
        console.log('error in migrating purchase (package probably isn\'t loaded):', ex);
      }
      changedSomething = true;
      window.localStorage.removeItem('shopBoughtfruits' + i);
    }
  }
  if (changedSomething) {
    refresh();
  }
});
// end of migration